## [1.3.1](https://gitlab.com/Martin_Huecklekemkes/release-demo/compare/v1.3.0...v1.3.1) (2024-01-25)


### Bug Fixes

* **ci config:** rename branch name, so unit tests also run on commits to dev ([b417539](https://gitlab.com/Martin_Huecklekemkes/release-demo/commit/b417539dccf427c5d3cd7e04dfe095094b678272))

# [1.3.0](https://gitlab.com/Martin_Huecklekemkes/release-demo/compare/v1.2.0...v1.3.0) (2024-01-25)


### Features

* add release info to readme ([8b39457](https://gitlab.com/Martin_Huecklekemkes/release-demo/commit/8b394577c721326283e08533cd0676b31ed76a31))

# [1.2.0](https://gitlab.com/Martin_Huecklekemkes/release-demo/compare/v1.1.0...v1.2.0) (2024-01-25)


### Features

* update README.md ([2c78df7](https://gitlab.com/Martin_Huecklekemkes/release-demo/commit/2c78df7155cbf8b66d32107c9f9ed410487c6ac0))

# [1.1.0](https://gitlab.com/Martin_Huecklekemkes/release-demo/compare/v1.0.0...v1.1.0) (2024-01-25)


### Features

* add release info to readme ([deb023e](https://gitlab.com/Martin_Huecklekemkes/release-demo/commit/deb023eb522600c489a21622a8eac2fef9ac14de))

# 1.0.0 (2024-01-25)


### Bug Fixes

* .gitlab-ci.yml ([ecc1d90](https://gitlab.com/Martin_Huecklekemkes/release-demo/commit/ecc1d90595914aad1202f04e7bc7895eb66d0488))
* .gitlab-ci.yml ([132a738](https://gitlab.com/Martin_Huecklekemkes/release-demo/commit/132a73819e9dd110dd33533b8fd7ccf770bfab8a))
* fix .gitlab-ci.yml not running release stage ([1bbe52b](https://gitlab.com/Martin_Huecklekemkes/release-demo/commit/1bbe52bd592d56596a87745b4ad93876c656052b))


### Features

* setup readme ([002d773](https://gitlab.com/Martin_Huecklekemkes/release-demo/commit/002d773089e0e61f533b859936bd2fd98dca40c5))
